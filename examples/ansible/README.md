# Sample Mikrotik configuration script

1. Put your vars and passwords into hosts.yml

2. Dump your config into KeePass DB.

```
bares --source-file=./hosts.yml dump keepass:~/Documents/mikrotik.kdbx?mikrotik#ansible
```

3. Check your ~/Documents/mikrotik.kdbx file if it has entry `mikrotik` and property `ansible`

4. Remove plain config file hosts.yml

5. Run `./setup.yml` to configure your router using encrypted inventory (check the first line)
