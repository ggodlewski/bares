/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n country=poland disabled=no \
    distance=indoors frequency=auto installation=indoor mode=\
    ap-bridge ssid={{ wifi_ssid }} hide-ssid=yes wireless-protocol=802.11
/interface wireless security-profiles
set [ find default=yes ] authentication-types=wpa-psk,wpa2-psk group-ciphers=\
    tkip,aes-ccm mode=dynamic-keys supplicant-identity=MikroTik \
    unicast-ciphers=tkip,aes-ccm wpa-pre-shared-key=koko1234 \
    wpa2-pre-shared-key={{ wifi_pass }}
