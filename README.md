# Bares - Backup and Restore tools

Backup addons for kopia.io

## TODO

Some function not implemented (see comments)

## Install

```
pip install bares --extra-index-url https://gitlab.com/api/v4/projects/41386875/packages/pypi/simple
```

## Usage

### Ubuntu

```
bares list dpkg
bares list docker
```

```
bares dump dpkg
bares dump docker
```

### Use KeePass to store sensitive data

```
USER=admin PASS=secret bares --env=USER,PASS dump keepass:./db.kdbx?localhost#env
eval $( bares restore keepass:./db.kdbx?localhost#env ) some_command_that_requirest_credentials

bares --source-file=./inventory.yml dump keepass:./db.kdbx?entry#ansible
bares restore keepass:./db.kdbx?entry#ansible > inventory.yml
```

### BackBlaze

```
B2_KEY_ID=XXX B2_KEY=YYY bares list b2
B2_KEY_ID=XXX B2_KEY=YYY bares dump b2://BUCKET-NAME
B2_KEY_ID=XXX B2_KEY=YYY bares create b2://NEW-BUCKET-NAME
```

or combine with KeePass:

```
B2_KEY_ID=XXX B2_KEY=YYY bares --env=USER,PASS dump keepass:./db.kdbx?localhost#b2
eval $( bares restore keepass:./db.kdbx?localhost#b2 ) bares list b2://ACCOUNT_ID
```

### Postgres

```
bares --docker-image postgres:14.3 --docker-network postgres list pgsql://postgres
bares --docker-network postgres list pgsql://postgres

bares create pgsql://user:pass@host/dbname
bares --root-pass root_password --docker-network postgres create pgsql://new_user:new_pass@postgres/new_db

bares --backup-dir /tmp/dump dump pgsql://user:pass@host/dbname
bares --docker-network postgres dump pgsql://postgres/dbname

bares --docker-network postgres shell pgsql://postgres
```

### MySql

```
bares list mysql
bares create mysql://user:pass@host/dbname

bares dump mysql://user:pass@host/dbname
bares restore mysql://user:pass@host/dbname
```

### Mongo

```
bares list mongodb
bares dump mongodb://user:pass@host/dbname

bares restore mongodb://user:pass@host/dbname
```

## Development

### Setup

```
pip install -r requirements.txt
```

### Build

```
python3 -m pip install --upgrade build
python3 -m build
```

### Local installation

```
pip install .
```

On Ubuntu use `DEB_PYTHON_INSTALL_LAYOUT=deb_system` - https://github.com/pypa/setuptools/issues/3269#issuecomment-1254507377)

```
DEB_PYTHON_INSTALL_LAYOUT=deb_system pip install .
```
