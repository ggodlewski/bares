#!/usr/bin/python3

import os
import sys
import subprocess
import getpass
import yaml

import requests
import base64
import json
from datetime import datetime
from optparse import OptionParser
from urllib.parse import urlparse
from pykeepass import PyKeePass, create_database, exceptions as pykeepass_exceptions
from getpass import getpass

parser = OptionParser()
parser.add_option("-d", "--backup-dir", dest="dirname",
                  help="use directory DIR for backups", metavar="DIR", default = os.path.join(os.getcwd(), 'dump'))
parser.add_option("--docker-network", dest="docker_network",
                  help="attach network NETWORK to docker container", metavar="NETWORK")
parser.add_option("--docker-opts", dest="docker_opts")
parser.add_option("--docker-image", dest="docker_image")
parser.add_option("--env", dest="env")
parser.add_option("--source-file", dest="source_file")
parser.add_option("--list", action="store_true", default=False, dest="ansible_list") # Ignore - param passed by ansible-playbook
parser.add_option("--host", dest="ansible_host") # Ignore - param passed by ansible-playbook
parser.add_option("--root-user", dest="root_user")
parser.add_option("--root-pass", dest="root_pass")

(options, args) = parser.parse_args()

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def usage():
    eprint("Usage bares [opts] cmd protocol://[user:pass@]host/dbname")
    exit(1)

if (options.ansible_host):
    args = ['restore']
if (options.ansible_list):
    args = ['restore']

if len(args) == 1 and 'BARES_LOCATION' in os.environ:
    args.append(os.environ['BARES_LOCATION'])
if len(args) < 2:
    usage()

cmd = args[0]

uri_to_parse = args[1]
if 'BARES_LOCATION' in os.environ:
    uri_to_parse = os.environ['BARES_LOCATION']
uri = urlparse(uri_to_parse if uri_to_parse.__contains__(":") else uri_to_parse+"://")

dirname=os.path.expanduser(os.path.abspath(options.dirname))

if not os.path.isdir(dirname):
    os.mkdir(dirname)

def docker_run(cmd, interactive = False):
#     print(cmd)
    if interactive:
        res = subprocess.run(cmd, shell=True, text=True, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
    else:
        res = subprocess.run(cmd, shell=True, capture_output=True, text=True)
        sys.stderr.write(res.stderr)
    return res.stdout

def generate_psql_opts(uri):
    root_user = options.root_user if options.root_user else 'postgres'
    psql_opts = ""
    psql_opts = psql_opts + " --username=" + (uri.username if uri.username else root_user)
    if uri.hostname:
        psql_opts = psql_opts + " --host=" + uri.hostname
    if uri.port:
        psql_opts = psql_opts + " --port=" + uri.port
    if uri.path:
        psql_opts = psql_opts + " --dbname=" + uri.path[1:]
    return psql_opts

def generate_pg_docker_opts(uri):
    root_pass = options.root_pass if options.root_pass else 'password'
    docker_image = options.docker_image if options.docker_image else "postgres:latest"
    docker_opts = options.docker_opts if options.docker_opts else ""
    docker_opts = docker_opts + (" -e PGPASSWORD=" + uri.password if uri.password else " -e PGPASSWORD=" + root_pass)
    if options.docker_network:
        docker_opts = docker_opts + " --network=" + options.docker_network
    docker_opts = docker_opts + " --volume=" + dirname + ":/dump"
    docker_opts = docker_opts + " --rm "
    docker_opts = docker_opts + " -i " + docker_image
    return docker_opts

def generate_mysql_opts(uri):
    root_user = options.root_user if options.root_user else 'root'
    root_pass = options.root_pass if options.root_pass else 'password'
    mysql_opts = ""
    mysql_opts = mysql_opts + " --user=" + (uri.username if uri.username else root_user)
    mysql_opts = mysql_opts + " --password=" + (uri.password if uri.password else root_pass)
    if uri.hostname:
        mysql_opts = mysql_opts + " --host=" + uri.hostname
    if uri.port:
        mysql_opts = mysql_opts + " --port=" + uri.port
    if uri.path:
        mysql_opts = mysql_opts + " --database=" + uri.path[1:]
    return mysql_opts

def generate_mysql_docker_opts(uri):
    root_pass = options.root_pass if options.root_pass else 'password'
    docker_image = options.docker_image if options.docker_image else "mariadb:latest"
    docker_opts = options.docker_opts if options.docker_opts else ""
    docker_opts = docker_opts + (" -e MARIADB_PASSWORD=" + uri.password if uri.password else " -e MARIADB_PASSWORD=" + root_pass)
    if options.docker_network:
        docker_opts = docker_opts + " --network=" + options.docker_network
    docker_opts = docker_opts + " --volume=" + dirname + ":/dump"
    docker_opts = docker_opts + " --rm "
    docker_opts = docker_opts + " -i " + docker_image

    return docker_opts

def generate_mongo_docker_opts():
    docker_image = options.docker_image if options.docker_image else "mongo:latest"
    docker_opts = options.docker_opts if options.docker_opts else ""
    if options.docker_network:
        docker_opts = docker_opts + " --network=" + options.docker_network
    docker_opts = docker_opts + " --volume=" + dirname + ":/dump"
    docker_opts = docker_opts + " --rm "
    docker_opts = docker_opts + " -i " + docker_image
    return docker_opts

def generate_mongo_opts():
    mongo_opts = "mongodb://"
    if uri.username:
        mongo_opts = mongo_opts + uri.username
    if uri.password:
        mongo_opts = mongo_opts + ":" + uri.password
    if uri.hostname:
        mongo_opts = mongo_opts + "@" + uri.hostname
    if uri.port:
        mongo_opts = mongo_opts + ":" + uri.port
    if uri.path:
        mongo_opts = mongo_opts + "/" + uri.path[1:]
    return mongo_opts

def b2_authorize():
    if not 'B2_KEY_ID' in os.environ:
        raise Exception("Missing env var: \"B2_KEY_ID\"")
    if not 'B2_KEY' in os.environ:
        raise Exception("Missing env var: \"B2_KEY\"")

    id_and_key = os.environ['B2_KEY_ID'] + ":" + os.environ['B2_KEY']

    basic_auth_string = 'Basic ' + base64.b64encode(bytes(id_and_key, "UTF-8")).decode("UTF-8")
    headers = { 'Authorization': basic_auth_string }

    response = requests.request("GET", "https://api.backblazeb2.com/b2api/v2/b2_authorize_account", headers=headers)
    return response.json()

def ansible_list(obj):
    for key in obj:
        if obj[key]['hosts']:
            hosts = []
            for host_key in obj[key]['hosts']:
                if obj[key]['hosts'][host_key] == None:
                    hosts.append(host_key)
            obj[key]['hosts'] = hosts
    return json.dumps(obj)

class Handler:
    def docker_dump(self, uri):
        res = docker_run("docker ps")
        f = open(os.path.join(dirname, "docker.txt"), "w")
        f.write(res)
        f.close()
    def dpkg_dump(self, uri):
        res = subprocess.run("dpkg --get-selections | cut -f1 -d'\t'", shell=True, capture_output=True, text=True)
        f = open(os.path.join(dirname, "dpkg.txt"), "w")
        f.write(res.stdout)
        f.close()

    def pgsql_list(self, uri):
        psql_opts = generate_psql_opts(uri)
        docker_opts = generate_pg_docker_opts(uri)

        res = docker_run("echo 'select datname from pg_database;' | docker run " + docker_opts + " psql --tuples-only " + psql_opts + " | sed 's/ *//g'")
        print(res)

    def pgsql_shell(self, uri):
        psql_opts = generate_psql_opts(uri)
        docker_opts = generate_pg_docker_opts(uri)
        if sys.stdin.isatty():
            docker_run("docker run -it " + docker_opts + " psql --tuples-only " + psql_opts, interactive = True)
        else:
            docker_run("docker run -i " + docker_opts + " psql --tuples-only " + psql_opts, interactive = True)

    def pgsql_create(self, uri):
        root_user = options.root_user if options.root_user else 'postgres'
        root_pass = options.root_pass if options.root_pass else 'password'

        uri_connect = urlparse(uri.scheme + '://' + root_user + ':' + root_pass + '@' + uri.hostname) # omit database name in connect
        psql_opts = generate_psql_opts(uri_connect)
        docker_opts = generate_pg_docker_opts(uri_connect)
        db_name = uri.path[1:]

        sql = 'CREATE USER "' + uri.username + '" WITH LOGIN '
        res = docker_run("echo " + sql + " | docker run " + docker_opts + " psql --tuples-only " + psql_opts)
        sql = 'ALTER USER "' + uri.username + '" WITH PASSWORD \'' + uri.password + '\' '
        res = docker_run("echo \"" + sql + "\" | docker run " + docker_opts + " psql --tuples-only " + psql_opts)

        sql = 'CREATE DATABASE "' + db_name + '" WITH OWNER="' + uri.username + '" '
        res = docker_run("echo " + sql + " | docker run " + docker_opts + " psql --tuples-only " + psql_opts)

    def pgsql_dump(self, uri):
        psql_opts = generate_psql_opts(uri)
        docker_opts = generate_pg_docker_opts(uri)

        db_name = uri.path[1:]
        docker_run("docker run " + docker_opts + " rm -rf /dump/" + db_name)
        res = docker_run("docker run " + docker_opts + " pg_dump --format=directory " + psql_opts + " -f /dump/" + db_name)

    def pgsql_restore(self, uri):
        psql_opts = generate_psql_opts(uri)
        docker_opts = generate_pg_docker_opts(uri)

        db_name = uri.path[1:]
        res = docker_run("docker run " + docker_opts + " pg_restore--clean --format=directory " + psql_opts + " -f /dump/" + db_name)

    def mongo_list(self, uri):
        docker_opts = generate_mongo_docker_opts()
        mongo_opts = generate_mongo_opts()
        res = docker_run("echo 'show dbs;' | docker run " + docker_opts + " /usr/bin/mongo --quiet --norc " + mongo_opts + " | cut -f1 -d' '")
        print(res)

    def mongo_dump(self, uri):
        docker_opts = generate_mongo_docker_opts()
        mongo_opts = generate_mongo_opts()
        db_name = uri.path[1:]
        res = docker_run("echo 'show dbs;' | docker run " + docker_opts + " /usr/bin/mongodump --uri=" + mongo_opts + " --out=/dump")

    def mongo_restore(self, uri):
        docker_opts = generate_mongo_docker_opts()
        mongo_opts = generate_mongo_opts()
        db_name = uri.path[1:]
        res = docker_run("echo 'show dbs;' | docker run " + docker_opts + " /usr/bin/mongorestore --uri=" + mongo_opts + " /dump")

    def mysql_list(self, uri):
        mysql_opts = generate_mysql_opts(uri)
        docker_opts = generate_mysql_docker_opts(uri)

        res = docker_run("echo 'SHOW DATABASES;' | docker run " + docker_opts + " mysql " + mysql_opts + " | sed 's/ *//g' | tail -n +2 | grep -v '^$' ")
        print(res)

    def mysql_create(self, uri):
        root_user = options.root_user if options.root_user else 'root'
        root_pass = options.root_pass if options.root_pass else 'password'

        uri_connect = urlparse(uri.scheme + '://' + root_user + ':' + root_pass + '@' + uri.hostname) # omit database name in connect
        mysql_opts = generate_mysql_opts(uri_connect)
        docker_opts = generate_mysql_docker_opts(uri_connect)
        db_name = uri.path[1:]

        sql = 'CREATE USER IF NOT EXISTS "' + uri.username + '" '
        res = docker_run("echo " + sql + " | docker run " + docker_opts + " mysql " + mysql_opts)
        sql = 'SET PASSWORD FOR "' + uri.username + '" = PASSWORD(\'' + uri.password + '\') '
        res = docker_run("echo \"" + sql + "\" | docker run " + docker_opts + " mysql " + mysql_opts)

        sql = 'CREATE DATABASE IF NOT EXISTS "' + db_name + '" '
        res = docker_run("echo " + sql + " | docker run " + docker_opts + " mysql " + mysql_opts)

        sql = 'GRANT ALL PRIVILEGES ON ' + db_name + '.* TO \'' + uri.username + '\'@\'%\'; FLUSH PRIVILEGES; '
        res = docker_run("echo \"" + sql + "\" | docker run " + docker_opts + " mysql " + mysql_opts)

    def mysql_dump(self, uri):
        uri_connect = urlparse(uri.scheme + '://' + uri.username + ':' + uri.password + '@' + uri.hostname) # omit database name in connect

        mysql_opts = generate_mysql_opts(uri_connect)
        docker_opts = generate_mysql_docker_opts(uri)

        db_name = uri.path[1:]

        res = docker_run("docker run " + docker_opts + " mysqldump --single-transaction --routines --triggers --add-drop-table --extended-insert --result-file=/dump/mysql.sql " + mysql_opts + ' ' + db_name)

    def mysql_restore(self, uri):
        uri_connect = urlparse(uri.scheme + '://' + uri.username + ':' + uri.password + '@' + uri.hostname) # omit database name in connect

        mysql_opts = generate_mysql_opts(uri_connect)
        docker_opts = generate_mysql_docker_opts(uri)

        db_name = uri.path[1:]

        res = docker_run("docker run " + docker_opts + " mysql " + mysql_opts + ' ' + db_name + ' /dump/mysql.sql')

    def mysql_shell(self, uri):
        mysql_opts = generate_mysql_opts(uri)
        docker_opts = generate_mysql_docker_opts(uri)
        if sys.stdin.isatty():
            docker_run("docker run -it " + docker_opts + " mysql " + mysql_opts, interactive = True)
        else:
            docker_run("docker run -i " + docker_opts + " mysql " + mysql_opts, interactive = True)

    def dumps_gc(self, uri):
        print("find /backup -type f -mtime +2 -exec rm {} \;")

    def home_gc(self, uri):
        print(docker_run("find $HOME/Downloads/ -type f -mtime +3 -delete"))
        print(docker_run("find $HOME/Downloads/ -type d -empty -mtime +3 -delete"))
        print(docker_run("find $HOME/Desktop/ -type f -mtime +3 -exec rm {} \;"))
        print(docker_run("find $HOME -maxdepth 1 -type f -mtime +3 -and -not -path '*/\.*' -exec rm {} \;"))

    def keepass_restore(self, uri):
        if 'BARES_PASS' in os.environ:
            password = os.environ['BARES_PASS']
        else:
            password = getpass('KeePass Password: ')
        file_path = os.path.expanduser(uri.path)
        kp = PyKeePass(file_path, password=password)
        entry_name = uri.query
        entry = kp.find_entries(title=entry_name, group=kp.root_group, recursive=False, first=True)
        if not entry:
            raise Exception("Entry: \"" + entry_name + "\" not found")

        property = entry.get_custom_property(uri.fragment)
        if property:
            if (options.ansible_host):
                obj = yaml.safe_load(property)
                eprint("options.ansible_host", options.ansible_host)
                json.dumps(obj[options.ansible_host]['vars'])
            elif (options.ansible_list):
                obj = yaml.safe_load(property)
                print(ansible_list(obj))
            else:
                print(property)
        else:
            raise Exception("Property: \"" + uri.fragment + "\" not found")

    def keepass_dump(self, uri):
        if 'BARES_PASS' in os.environ:
            password = os.environ['BARES_PASS']
        else:
            password = getpass('KeePass Password: ')
        file_path = os.path.expanduser(uri.path)
        if not os.path.exists(file_path):
            create_database(file_path, password=password)
        kp = PyKeePass(file_path, password=password)

        entry_name = uri.query
        entry = kp.find_entries(title=entry_name, group=kp.root_group, recursive=False, first=True)
        if not entry:
            entry = kp.add_entry(kp.root_group, title=entry_name, username='', password='')

        if options.env:
            env_keys = options.env.split(',')
            txt = ''
            for env_key in env_keys:
                txt += env_key + '="' + os.environ[env_key].replace('"', '\\"') + '" '
            entry.set_custom_property(uri.fragment, txt)

        if options.source_file:

            with open(os.path.expanduser(options.source_file)) as f:
                lines = f.readlines()
                txt = ''.join(lines)
                entry.set_custom_property(uri.fragment, txt)

                try:
                    data = yaml.safe_load(txt)
                    for key in data:
                        if data[key]['vars']:
                            if data[key]['vars']['ansible_user']:
                                entry.username = data[key]['vars']['ansible_user']
                            if data[key]['vars']['ansible_password']:
                                entry.password = data[key]['vars']['ansible_password']
                except:
                    pass

#         attachments = kp.find_attachments(filename='hello.txt')
#         if attachments:
#             for attachment in attachments:
#                 kp.delete_binary(attachment.id)
#         binary_id = kp.add_binary(b'Hello world')
#         entry.add_attachment(binary_id, filename='hello.txt')
        kp.save()

    def b2_list(self, uri):
        auth_json = b2_authorize()

        headers = { 'Authorization': auth_json['authorizationToken'] }
        payload = json.dumps({ 'accountId' : auth_json['accountId'], 'bucketTypes': ['allPrivate', 'allPublic'] })
        response = requests.request("POST", auth_json['apiUrl'] + "/b2api/v2/b2_list_buckets", headers=headers, data=payload)
        response_json = response.json()
        for bucket in response_json['buckets']:
            print(bucket['bucketId'], bucket['bucketName'], bucket['bucketType'], sep="\t")

    def b2_dump(self, uri):
        bucketName = uri.netloc
        if not bucketName:
            raise Exception("No bucket name")

        auth_json = b2_authorize()

        headers = { 'Authorization': auth_json['authorizationToken'] }
        payload = json.dumps({ 'accountId' : auth_json['accountId'], 'bucketTypes': ['allPrivate', 'allPublic'] })
        response = requests.request("POST", auth_json['apiUrl'] + "/b2api/v2/b2_list_buckets", headers=headers, data=payload)
        response_json = response.json()

        bucketId = None
        for bucket in response_json['buckets']:
            if bucket['bucketName'] == bucketName:
                bucketId = bucket['bucketId']

        headers = { 'Authorization': auth_json['authorizationToken'] }
        payload = json.dumps({ 'bucketId' : bucketId, 'maxFileCount': 10000 })
        response = requests.request("POST", auth_json['apiUrl'] + "/b2api/v2/b2_list_file_names", headers=headers, data=payload)
        response_json = response.json()
        total = 0
        for file in response_json['files']:
            print(file['fileName'], file['contentLength'], file['contentSha1'], datetime.fromtimestamp(file['uploadTimestamp'] / 1000, tz=None), sep="\t")
            total += file['contentLength']
        print("@TOTAL@", total, "%.1f MB" % (total / 1024 / 1024), sep="\t")

    def b2_create(self, uri):
        bucketName = uri.netloc
        if not bucketName:
            raise Exception("No bucket name")

        auth_json = b2_authorize()

        headers = { 'Authorization': auth_json['authorizationToken'] }
        payload = json.dumps({ 'accountId' : auth_json['accountId'], 'bucketTypes': ['allPrivate', 'allPublic'] })
        response = requests.request("POST", auth_json['apiUrl'] + "/b2api/v2/b2_list_buckets", headers=headers, data=payload)
        response_json = response.json()

        headers = { 'Authorization': auth_json['authorizationToken'] }
        payload = json.dumps({ 'accountId' : auth_json['accountId'], 'bucketName': bucketName, 'bucketType': 'allPrivate' })
        response = requests.request("POST", auth_json['apiUrl'] + "/b2api/v2/b2_create_bucket", headers=headers, data=payload)
        if response.status_code != 200:
            print(response.json()['message'])
            exit(1)

        print('Bucket created')

def main_cli():
    handler = Handler()
    try:
        funcname = uri.scheme + '_' + cmd
        func = getattr(handler, funcname)
    except AttributeError:
        usage()
    else:
        try:
            func(uri)
        except pykeepass_exceptions.CredentialsError:
            eprint("Incorrect KeePass credentials")
            exit(1)

if __name__ == '__main__':
    main_cli()
